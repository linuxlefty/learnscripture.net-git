from django.dispatch import Signal

new_award = Signal()
lost_award = Signal()
