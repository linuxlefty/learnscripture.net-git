{% load url from future %}{% autoescape off %}
Hi {{ user.username }},

You're receiving this e-mail because you requested a password
reset for your account on learnscripture.net. (Just ignore
this email if you didn't).

Please go to the following page and choose a new password:

{{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}

Thanks for using our site!

The learnscripture.net team

{% endautoescape %}
